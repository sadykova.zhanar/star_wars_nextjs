import React from "react";
import { Spin, Space } from "antd";
import styles from "../styles/Home.module.css";

function Loader() {
  return (
    <div className={styles.overlay}>
      <Space
        size="middle"
        style={{ width: "3rem", height: "3rem" }}
        className={styles.Loader}
      >
        <Spin size="large" />
      </Space>
    </div>
  );
}
export default Loader;
