import React, { useState } from "react";
import Link from "next/link";
import { Menu } from "antd";

export default function Toolbar() {
  const [current, setCurrent] = useState("home");
  const handleClick = (e: any) => {
    setCurrent(e.key);
  };
  return (
    <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal"  theme="dark">
      <Menu.Item key="home">
        <Link href="/">
          <a>Home page</a>
        </Link>
      </Menu.Item>
      <Menu.Item key="planets">
        <Link href="/planets/planets">
          <a>Planets</a>
        </Link>
      </Menu.Item>
      <Menu.Item key="heroes">
        <Link href="/heroes/heroes">
          <a>Heroes</a>
        </Link>
      </Menu.Item>
    </Menu>
  );
}
