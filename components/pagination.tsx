import React, { useEffect } from "react";
import PaginationItem from "./PaginationItem";

interface PaginationProps {
  currPage: number;
  changePage: (page: number) => any;
  changePageRange: (indexOfPageRange: number) => any;
  indexOfPageRange: number;
  pageCount: number;
}

function useKeyPressWithControl(targetKey: string, cb: () => void) {
  useEffect(() => {
    let pressed: string[] = [];
    function handlekeydownEvent(event: any) {
      pressed.push(event.key);
      if (
        (pressed.includes("Control") || pressed.includes("Meta")) &&
        pressed.includes(targetKey)
      ) {
        cb();
      }
    }
    function handlekeyUpEvent(event: any) {
      pressed = pressed.filter((item) => item !== event.key);
    }
    document.addEventListener("keydown", handlekeydownEvent);
    document.addEventListener("keyup", handlekeyUpEvent);
    return () => {
      document.removeEventListener("keydown", handlekeydownEvent);
      document.removeEventListener("keyup", handlekeyUpEvent);
    };
  }, [targetKey, cb]);
}

function Pagination({
  currPage,
  changePage,
  changePageRange,
  indexOfPageRange,
  pageCount,
}: PaginationProps) {
 
  const pageRange = Math.ceil(pageCount / 10);

  const pageArray: Array<number[]> = [];
  let arr = [];
  let count = 0;
  for (let i = 1; i <= pageCount; i++) {
    arr.push(i);
    count++;
    if (count === pageRange) {
      pageArray.push(arr);
      count = 0;
      arr = [];
    }
  }
  if (pageArray.length % 2 !== 0) {
    pageArray[pageRange - 1].push(pageCount);
  }

  useKeyPressWithControl("ArrowRight", () => {
    if (
      pageArray[indexOfPageRange + 1] !== undefined &&
      currPage + 1 === pageArray[indexOfPageRange + 1][0]
    ) {
      nextPageRange(currPage + 1);
    } else {
      nextPage(currPage + 1);
    }
  });

  useKeyPressWithControl("ArrowLeft", () => {
    if (
      pageArray[indexOfPageRange - 1] !== undefined &&
      currPage - 1 ===
        pageArray[indexOfPageRange - 1][
          pageArray[indexOfPageRange - 1].length - 1
        ]
    ) {
      prevPageRange(currPage - 1);
    } else {
      prevPage(currPage - 1);
    }
  });

  const prevPageRange = (page: number) => {
    changePage(page);
    changePageRange(indexOfPageRange - 1);
  };

  const nextPageRange = (page: number) => {
    if (page === pageCount) {
      changePage(page);
      changePageRange(pageArray.length - 1);
    } else {
      changePage(page);
      changePageRange(indexOfPageRange + 1);
    }
  };

  const prevPage = (page: number) => {
    if (currPage !== 1) {
      if (page === currPage) {
        prevPageRange(currPage - 1);
      } else {
        changePage(currPage - 1);
      }
    }
  };

  const nextPage = (page: number) => {
    if (currPage !== pageCount) {
      if (page === currPage) {
        nextPageRange(currPage + 1);
      } else {
        changePage(currPage + 1);
      }
    }
  };

  const getPages = () => {
    if (indexOfPageRange > 0 && indexOfPageRange < pageArray.length - 1) {
      let prevArrofPages: number[] = pageArray[indexOfPageRange - 1].splice(
        0,
        Math.ceil(pageRange / 2)
      );
      let currArrofPages: number[] = pageArray[indexOfPageRange];
      return (
        <>
          {prevArrofPages.map((page) => {
            return (
              <PaginationItem
                key={page}
                pageNumber={page}
                currPage={currPage}
                onClick={() => {
                  if (prevArrofPages.includes(page)) {
                    prevPageRange(page);
                  } else {
                    changePage(page);
                  }
                }}
              />
            );
          })}
          <PaginationItem
            pageNumber="..."
            onClick={() =>
              prevPageRange(
                pageArray[indexOfPageRange - 1][
                  pageArray[indexOfPageRange - 1].length - 1
                ]
              )
            }
            currPage={currPage}
          />
          {currArrofPages.map((page) => {
            return (
              <PaginationItem
                key={page}
                pageNumber={page}
                currPage={currPage}
                onClick={() => changePage(page)}
              />
            );
          })}
          <PaginationItem
            pageNumber="..."
            onClick={() => nextPageRange(pageArray[indexOfPageRange + 1][0])}
            currPage={currPage}
          />
          <PaginationItem
            pageNumber={pageCount}
            onClick={() => nextPageRange(pageCount)}
            currPage={currPage}
          />
        </>
      );
    } else {
      return (
        <>
          {indexOfPageRange !== 0 ? (
            <PaginationItem
              pageNumber="..."
              onClick={() =>
                prevPageRange(pageArray[indexOfPageRange - 1][pageRange - 1])
              }
              currPage={currPage}
            />
          ) : null}
          {pageArray.length !== 0
            ? pageArray[indexOfPageRange].map((page) => {
                return (
                  <PaginationItem
                    key={page}
                    pageNumber={page}
                    currPage={currPage}
                    onClick={() => changePage(page)}
                  />
                );
              })
            : null}
          {indexOfPageRange !== pageArray.length - 1 ? (
            <PaginationItem
              pageNumber="..."
              onClick={() => nextPageRange(pageArray[indexOfPageRange + 1][0])}
              currPage={currPage}
            />
          ) : null}
        </>
      );
    }
  };

  return (
    <div style={{ marginBottom: "20px", textAlign:"center" }}>
      {getPages()}
    </div>
  );
}

export default Pagination;
