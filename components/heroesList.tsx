import React from "react";
import type { HeroType } from "../pages/heroes/heroes";
import { List } from "antd";
import Link from "next/link";

interface AllHeroesProps {
  heroes: Array<HeroType>;
}

function HeroesList({ heroes }: AllHeroesProps) {
  return (
    <List
      size="large"
      bordered
      dataSource={heroes}
      renderItem={(item) => (
        <List.Item>
          <Link href={`/heroes/${item.url.split("/")[5]}`}>
            <a>{item.name}</a>
          </Link>
        </List.Item>
      )}
    />
  );
}
export default HeroesList;
