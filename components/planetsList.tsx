import React from "react";
import type { PlanetType } from "../pages/planets/planets";
import { List } from "antd";
import Link from "next/link";

interface AllPlanetsProps {
  planets: Array<PlanetType>;
}

function PlanetList({ planets }: AllPlanetsProps) {
  return (
    <List
      size="large"
      bordered
      dataSource={planets}
      renderItem={(item) => (
        <List.Item>
          <Link href={`/planets/${item.url.split("/")[5]}`}>
            <a>{item.name}</a>
          </Link>
        </List.Item>
      )}
    />
  );
}
export default PlanetList;
