import axios from "../axios-base";

export async function fetcher(url: string) {
  const response = await axios.get(url);
  return await response.data;
}

export function getPageNum(currPage: number) {
  return currPage % 10 === 0 ? (currPage % 10) + 1 : currPage % 10;
}

export async function getAllHeroesId() {
  try {
    const heroes = await axios.get("/people/");
    let ids = [];
    for (let i = 1; i <= heroes.data.count + 1; i++) {
      ids.push({
        params: {
          id: i.toString(),
        },
      });
    }
    return ids;
  } catch (e) {
    console.log(e);
  }
}

export async function getPagesCount() {
  try {
    const heroes = await axios.get("/people/");
    return heroes.data.count;
  } catch (e) {
    console.log(e);
  }
}

export async function getHeroData(id: string) {
  try {
    const hero = await axios.get(`/people/${id}/`);
    return {
      id,
      ...hero.data,
    };
  } catch (e) {
    console.log(e);
  }
}
