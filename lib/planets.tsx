import axios from "../axios-base";

export async function fetcher(url: string) {
  const response = await axios.get(url);
  return await response.data;
}
export function getPageNum(currPage: number) {
  let pageNum = currPage % 10;
  switch (pageNum) {
    case 7:
    case 8:
    case 9:
      pageNum = 6;
      break;
    case 0:
      pageNum = 1;
      break;
    default:
      break;
  }
  return pageNum;
}

export async function getAllPlanetsId() {
  try {
    const planets = await axios.get("/planets/");
    let ids = [];
    for (let i = 1; i <= planets.data.count; i++) {
      ids.push({
        params: {
          id: i.toString(),
        },
      });
    }
    return ids;
  } catch (e) {
    console.log(e);
  }
}

export async function getPagesCount() {
  try {
    const planets = await axios.get("/planets/");
    return planets.data.count;
  } catch (e) {
    console.log(e);
  }
}
export async function getPlanetData(id: string) {
  try {
    const planet = await axios.get(`/planets/${id}/`);
    return {
      id,
      ...planet.data,
    };
  } catch (e) {
    console.log(e);
  }
}
