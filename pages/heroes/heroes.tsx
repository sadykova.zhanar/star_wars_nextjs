import React, { useState } from "react";
import useSWR from "swr";
import { GetStaticProps } from "next";
import HeroesList from "../../components/heroesList";
import Loader from "../../components/loader";
import Layout from "../../components/layout";
import { fetcher, getPageNum, getPagesCount } from "../../lib/heroes";
import styles from "../../styles/Home.module.css";

export interface HeroesResponse {
  count: number;
  next: string | null;
  previous: string | null;
  results: Array<HeroType>;
}
export interface HeroType {
  name: string;
  height: string;
  mass: string;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
  homeworld: string;
  films: string[];
  species: string[];
  vehicles: string[];
  starships: string[];
  created: string;
  edited: string;
  url: string;
}

export const getStaticProps: GetStaticProps = async () => {
  const pagesCount = await getPagesCount();
  return {
    props: {
      pagesCount,
    },
  };
};

export default function Heroes({ pagesCount }: { pagesCount: number }) {
  const [currPage, setCurrPage] = useState(1);
  const [indexOfPageRange, setindexOfPageRange] = useState(0);

  const { data: heroes, error } = useSWR<HeroesResponse>(
    `/people/?page=${getPageNum(currPage)}`,
    fetcher
  );
  if (error) return <div>failed to load</div>;

  return (
    <Layout
      info
      currPage={currPage}
      indexOfPageRange={indexOfPageRange}
      changePage={(page) => {
        setCurrPage(page);
      }}
      changePageRange={(index) => {
        setindexOfPageRange(index);
      }}
      pageCount={pagesCount}
    >
      {!heroes ? (
        <Loader />
      ) : (
        <div className={styles.description}>
          Heroes
          <HeroesList heroes={heroes.results} />
        </div>
      )}
    </Layout>
  );
}
