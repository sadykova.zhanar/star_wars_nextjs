import { getAllHeroesId, getHeroData } from "../../lib/heroes";
import { HeroType } from "./heroes";
import { Card } from "antd";
import { GetStaticProps, GetStaticPaths } from "next";
import Layout from "../../components/layout";

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = await getAllHeroesId();
  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const heroData = await getHeroData(params.id as string);
  return {
    props: {
      heroData,
    },
  };
};

export default function Hero({ heroData }: { heroData: HeroType }) {
  return (
    <Layout>
      <Card title={heroData.name}>
        <p>Birth year: {heroData.birth_year}</p>
        <p>Eye color: {heroData.eye_color}</p>
        <p>Gender: {heroData.gender}</p>
        <p>Hair color: {heroData.hair_color}</p>
        <p>Height: {heroData.height}</p>
      </Card>
    </Layout>
  );
}
