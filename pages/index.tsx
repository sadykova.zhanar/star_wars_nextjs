import Layout from "../components/layout";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <Layout>
      <div className={styles.title}>Welcome! Select Planets or Heroes.</div>
    </Layout>
  );
}
